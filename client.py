import http.client
import json
import threading
from colorama import Fore
import random


class Client:
    """ Client for chat room """

    def __init__(self):
        """ Create a Client"""
        self.connection = None
        self.name = None
        self.auth = None
        self.color_dictionary = dict()
        self._create_connection()
        self.run()

    def _create_connection(self):
        """ Create a connection for communication with Server """
        self.connection = http.client.HTTPConnection(host="127.0.0.1", port=8000)

    def join(self):
        """ Join to server """

        params = json.dumps({'name': self.name})

        headers = {}
        self.connection.request("POST", "/join", params, headers)

        # get response from server
        rsp = self.connection.getresponse()

        self.auth = rsp.headers['auth']

        if rsp.status == 200:
            print(Fore.WHITE + f"Welcome to chat room {self.name}")

    def send_message_thread(self):
        """ Thread for sending message to server """
        while True:
            message = input('')
            self.send_message(message)

    def send_message(self, message):
        """ Send message to server """

        params = json.dumps({'message': message})
        headers = {"auth": self.auth}
        self.connection.request("POST", "/send", params, headers)

        # get response from server
        rsp = self.connection.getresponse()

        data_received = rsp.read()

    def receive_message_thread(self):
        """ Thread for getting last message in server """
        while True:
            self.receive_message()

    def receive_message(self):
        """ Receive messages from other client via server """

        headers = {"auth": self.auth}
        self.connection.request("GET", "/receive", headers=headers)
        response = self.connection.getresponse()
        data_received = response.read()
        data = json.loads(data_received.decode())

        name = data['name']
        message = data['message']

        if name in self.color_dictionary:
            color = self.color_dictionary[name]
        else:
            self.color_dictionary[name] = random.choice([Fore.BLUE, Fore.CYAN, Fore.GREEN, Fore.YELLOW, Fore.MAGENTA,
                                                         Fore.LIGHTYELLOW_EX, Fore.LIGHTMAGENTA_EX, Fore.LIGHTCYAN_EX,
                                                         Fore.LIGHTBLUE_EX, Fore.LIGHTGREEN_EX, Fore.LIGHTRED_EX])
            color = self.color_dictionary[name]

        if name == 'Server':
            print(Fore.WHITE + message)
        else:
            print(color + f"{name}: {message}")

    def run(self):
        """ Run Client application"""

        self.name = input('Enter your name:')
        print()
        self.join()

        threading.Thread(target=self.send_message_thread).start()
        threading.Thread(target=self.receive_message_thread).start()

    def _exit(self):
        """ Exit client from the program """
        self.connection.close()


if __name__ == '__main__':
    client = Client()
