from http.server import HTTPServer, BaseHTTPRequestHandler
import random
import string
import json
import time
from socketserver import ThreadingMixIn


class ThreadingServer(ThreadingMixIn, HTTPServer):
    pass


def make_handler():
    """ This method add some variables to below class """
    client_messages = {}

    class SimpleRequestHandler(BaseHTTPRequestHandler):
        """ A custom HTTPRequestHandler """
        auth_dictionary = dict()

        def __init__(self, request, client_address, server):
            """ Create a ServerRequestHandler """
            super().__init__(request, client_address, server)

        def _set_response(self, client_auth):
            """ Set response for a request """
            self.send_response(200)
            if client_auth is not None:
                self.send_header('auth', client_auth)
            self.end_headers()

        def do_GET(self):
            """ Handle GET command """
            if self.path == "/receive":
                client_auth = self.headers['auth']
                self.send_response(200)
                self._set_response(None)
                message = None
                try:
                    while len(client_messages[client_auth]) == 0:
                        time.sleep(0.001)

                    message = client_messages[client_auth].pop(0)
                    self.wfile.write(message.encode('utf-8'))
                except ConnectionResetError:
                    client_messages[client_auth].insert(0, message)

        def do_POST(self):
            """ Handle POST command """
            # Path of request
            path = self.path

            # Size of body
            content_length = int(self.headers['Content-Length'])

            # Data of body
            post_data = self.rfile.read(content_length)

            if path == "/join":
                post_data_json = json.loads(post_data.decode('utf-8'))
                user_name = post_data_json['name']
                print()
                self.join_user(user_name)
                self._set_response(client_auth=self.get_auth(user_name))
                self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

            if path == "/send":
                post_data_json = json.loads(post_data.decode('utf-8'))
                user_auth = self.headers['auth']
                user_name = self.get_client_name(user_auth)
                message = post_data_json['message']
                for auth in self.auth_dictionary:
                    if user_auth != auth:
                        new_item = json.dumps({'name': user_name, 'message': message})
                        client_messages[auth].append(new_item)
                self._set_response(client_auth=None)

        @staticmethod
        def generate_random_string():
            """
            Generate a random string for authentication

            :return: a random string
            """
            letters = string.ascii_lowercase
            result_str = ''.join(random.choice(letters) for i in range(10))
            return result_str

        def get_auth(self, client_name):
            """
            Get auth string via name of client
            :param client_name: name of the client
            :return: auth of the client
            """
            if client_name in self.auth_dictionary.values():
                for auth, name in self.auth_dictionary.items():
                    if name == client_name:
                        return auth
            else:
                new_auth = self.generate_random_string()
                client_messages[new_auth] = list()
                self.auth_dictionary[new_auth] = client_name
                return new_auth

        def exist_user_before(self, client_name):
            """ Check a user exist in before or not """
            if client_name in self.auth_dictionary.values():
                return True
            else:
                return False

        def get_client_name(self, client_auth):
            """
            Get client name via auth
            :param client_auth: auth of the client
            :return: name of the client
            """
            return self.auth_dictionary[client_auth]

        def join_user(self, user_name):
            if self.exist_user_before(user_name):
                return

            user_auth = self.get_auth(user_name)

            for auth in self.auth_dictionary:
                if user_auth != auth:
                    message = f"{user_name} joined the chat room!"
                    new_item = json.dumps({'name': 'Server', 'message': message})
                    client_messages[auth].append(new_item)

    # Return class with some variables
    return SimpleRequestHandler
