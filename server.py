from simple_request_handler import make_handler, ThreadingServer


class Server:
    """ Server for chat room """

    def __init__(self):
        """ Create a Client"""
        self.http_server = None
        self.create_http_server()
        self.run()

    def create_http_server(self):
        """ Create a HTTPServer for handling request """
        server_address = ('127.0.0.1', 8000)
        self.http_server = ThreadingServer(server_address, make_handler())

        print('http server is running...')

    def run(self):
        """ Run Client application """

        self.http_server.serve_forever()


if __name__ == '__main__':
    server = Server()
